# US stock data handling training

## はじめに
jupyter-labの対話型の実行環境を利用して、米国株価データを取得して、そのデータで解析ゴッコする。イケルと思ったら、その株を購入する。

## 全体の流れ
証券口座の開設、証券会社の取り扱い可能銘柄の確認、Tickerの確認と取り扱える銘柄をフィルタリングするスクリプトの設計・実装を行う

### 証券口座の開設
　今回は[「楽天証券」](https://www.rakuten-sec.co.jp/)を使う。特に他意はない、後述するCSVファイルを入手できることが大きい。実際の開設は各自で行う。

### 証券会社の取り扱い可能銘柄の確認
　[楽天証券](https://www.rakuten-sec.co.jp/)で取り扱っている銘柄は当該ウェブページで確認できるが、後述するCSVの形式でダウンロード可能である。

### データファイルの準備
　ここで言うデータとは、証券会社が取り扱っている銘柄や業種、Tickerがまとめられ、CSVファイルとしてダウンロード出来る形状のものと自分が取り扱いたい業種(sector)を指す。
1. 取扱銘柄のデータ
　データは[楽天証券からダウンロード](https://www.rakuten-sec.co.jp/web/market/search/us_search/result.html?all=on&vall=on&forwarding=na&target=0&theme=na&returns=na&head_office=na&name=&code=&sector=na&pageNo=&c=us&p=result&r1=on)する。ダウンロードした後にpythonで利用しやすいようにヘッダ名を日本語から英語に置き換える。ファイル名は加工後に識別しやすくするために、**YYYYMMDD_result_us.csv** とする。変更点に関しては実際のファイルを参照すること。今後、記載がない場合は実際のファイルが正となる。

2. 取扱業種設定
　自分の取り扱い業種を設定するためのファイル。全ての業種を取り扱いたい場合は全てをEnable(=1)にすること。ファイル名は **sector_handling.csv** 。


## 習作スクリプト
　上記が準備できたら、実際にロジックを設計して習作スクリプトとして実装する。今回、設計したスクリプトの命名規約として```trn```を付ける。各スクリプトにjupyter-labの特徴であるMarkdownで解説を付記していく。

1. trn_selectSector.ipynb
2. trn_selectTikcer.ipynb
3. trn_getPricebyTickers.ipynb

## 終わりに
　巷で出回っている「情報商材」の様な即効性はありませんがデータが手元にあればナニカの取っ掛かりにはなると思うので、これをキッカケに遊んでもらえれば幸甚です。

　
